#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools, AutoToolsBuildEnvironment

import os
from os import path

class Recipe(ConanFile):
    name        = "ns3"
    version     = "3.28"
    license     = "GPL 2.0"
    description = "ns-3 is a discrete-event network simulator for Internet systems"
    settings    = "os", "compiler", "build_type", "arch"

    homepage    = "https://www.nsnam.org/"
    url         = "https://gitlab.com/0-face/conan-recipes/ns3-conan"

    options = {
        'build_examples': [True, False],

        # Ns3 modules that will be linked. Ex.: "csma internet point-to-point",
        'modules': "ANY",

        # Use version dependent libnames (ex.: ns3.28-core) or
        # version independent aliases (ex.: ns3-core)
        'version_independent_libs' : [True, False]
    }
    default_options = (
        'build_examples=False',
        'modules=all',
        'version_independent_libs=True'
    )

    ''' List of modules supported by ns3 (in this version).

        * Some modules were commented out, because they require
            external dependencies (not configured/built by this script yet).
    '''
    ns3_modules = (
        'antenna', 'aodv', 'applications', 'bridge',
        # 'brite'*,
        'buildings', 'core',
        #'click'*,
        'config-store', 'csma', 'csma-layout',
        'dsdv', 'dsr', 'energy', 'fd-net-device', 'flow-monitor',
        'internet', 'internet-apps', 'lr-wpan', 'lte', 'mesh', 'mobility',
        'mpi', 'netanim', 'network', 'nix-vector-routing', 'olsr',
        # 'openflow'*,
        'point-to-point', 'point-to-point-layout',
        'propagation', 'sixlowpan', 'spectrum', 'stats', 'tap-bridge',
        'topology-read', 'traffic-control', 'uan',
        'virtual-net-device',
        #'visualizer*,
        'wave', 'wifi', 'wimax'
    )

    RELEASE_DIR = "ns-allinone-{}".format(version)
    RELEASE_NS3_DIR = "ns-{}".format(version)
    RELEASE_FILE = RELEASE_DIR + ".tar.bz2"
    RELEASE = {
        'source': "https://www.nsnam.org/release/" + RELEASE_FILE,
        'sha256': "cfa27e06d21b789d352563c7c0c95ab9167a5d7610e98b463978e1b9efac3323"
    }

    def source(self):
        tools.download(self.RELEASE['source'], self.RELEASE_FILE)
        tools.check_sha256(self.RELEASE_FILE, self.RELEASE['sha256'])
        tools.unzip(self.RELEASE_FILE)
        os.remove(self.RELEASE_FILE)

    def build(self):
        self.patch_sources()

        env_build = AutoToolsBuildEnvironment(self)

        with tools.environment_append(env_build.vars):
            self.run_and_log(
                    cmd = './waf configure build ' + self.waf_options(),
                    cwd = self.ns3_source_folder)

            self.run_and_log(
                    cmd = './waf install',
                    cwd = self.ns3_source_folder)

        self.create_libs_alias(lib_dir = path.join(self.install_dir, 'lib'))
        self._install_modules_info()

    def _install_modules_info(self):
        '''Extract info about modules dependencies from created pkgconfig files
            and stores them at a 'json' file, to avoid dependency from pkgconfig
            by package users.'''

        pkg_dir           = path.join(self.install_dir, 'lib', 'pkgconfig')
        modules_info_file = path.join(self.install_dir, 'deps.json')

        modules = self.collect_modules_info(pkg_dir)
        self.store_modules_dependencies(modules, modules_info_file)

    def package_id(self):
        # These options does not change what is built
        self.info.options.modules = None
        self.info.options.version_independent_libs = None

    def package(self):
        # Copies all 'install_dir' to package
        self.copy("*", src=self.install_dir, symlinks=True)

    def package_info(self):
        incdir = path.join('include', self.ns3_version_name)

        self.cpp_info.includedirs = [incdir]
        self.cpp_info.libs        = self.declare_libs_to_link()

        if self.settings.compiler in ["gcc", "clang"]:
            # This options causes the linker to use 'RPATH' instead of 'RUNPATH'
            # (See https://blog.qt.io/blog/2011/10/28/rpath-and-runpath/)
            # This way, the libraries '-rpath' are searched recursively
            ldflags = ["-Wl,--disable-new-dtags"]

            # With this option, the libraries not used by an executable (or library)
            # we not be included as a dependency in the produced file
            ldflags.append("-Wl,--as-needed")

            for f in ldflags:
                self.cpp_info.sharedlinkflags.append(f)
                self.cpp_info.exelinkflags.append(f)

    def patch_sources(self, strict=True):
        tools.replace_in_file(
            path.join(self.ns3_src_dir, 'lte', 'model', 'cqa-ff-mac-scheduler.cc'),
            "typedef std::map<HOL_group,std::set<LteFlowId_t> >::iterator t_it_HOLgroupToUEs",
            "typedef std::multimap<HOL_group,std::set<LteFlowId_t> >::iterator t_it_HOLgroupToUEs",
            strict=strict
        )



    #################################### Properties ###########################################

    @property
    def out_folder(self):
        return path.join(self.build_folder, 'build')

    @property
    def ns3_source_folder(self):
        return path.join(self.source_folder, self.RELEASE_DIR, self.RELEASE_NS3_DIR)

    @property
    def ns3_src_dir(self):
        return path.join(self.ns3_source_folder, 'src')

    @property
    def pkg_files_folder(self):
        return path.join(self.package_folder, 'lib', 'pkgconfig')

    @property
    def modules_deps_filepath(self):
        return  path.join(self.package_folder, 'deps.json')

    @property
    def ns3_version_name(self):
        if self.version == 'dev':
            return 'ns-dev'
        else:
            return 'ns' + self.version

    @property
    def install_dir(self):
        return path.abspath(path.join(self.build_folder, "install"))

    #################################### Helpers ###########################################

    def run_and_log(self, cmd, cwd=None):
        msg = ''
        if cwd:
            msg = "[{}] ".format(cwd)

        self.output.info(msg + cmd)

        self.run(cmd, cwd = cwd)

    def waf_options(self):
        opts = []

        opts.append("--out='{}'"   .format(self.out_folder))
        opts.append("--prefix='{}'".format(self.install_dir))

        # Build profile (debug or release)
        is_debug = (self.settings.build_type == "Debug")
        opts.append("--build-profile={}".format("debug" if is_debug else "release"))

        if self.options.build_examples:
            opts.append("--enable-examples")

        return " ".join(opts)


    #############################

    def collect_modules_info(self, pkgconfig_dir):
        modules = {}

        with tools.environment_append({'PKG_CONFIG_PATH': pkgconfig_dir}):
            for m in self.ns3_modules:
                module_pkg_name = self.ns3_module_to_pkg_name(m)
                pkg = tools.PkgConfig(module_pkg_name)

                modules[m] = Ns3Module(module_pkg_name, pkg.requires, pkg.libs_only_l)

        return modules


    def store_modules_dependencies(self, modules, dst):
        self.output.info('Saving modules dependencies to "%s"' % dst)

        import json

        modules_dict = {}

        for m_name, m in modules.iteritems():
            modules_dict[m_name] = m.to_dict()

        with open(dst, 'w') as out_file:
            json.dump(modules_dict, out_file, sort_keys=True, indent=4)

    def ns3_module_to_pkg_name(self, name):
        return "lib{}-{}".format(self.ns3_version_name, name)

    #############################

    def declare_libs_to_link(self):
        '''Extract info from built libraries (using pkg-config)
            to declare required libs.
        '''

        modules = self.list_used_modules()

        self.output.info("Modules to use: " + ' '.join(modules.keys()))

        return self.list_libs_from_modules(modules)

    def list_used_modules(self):
        modules = self.load_modules_dependencies()

        return self.filter_modules_from_options(modules)

    def load_modules_dependencies(self):
        import json

        with open(self.modules_deps_filepath) as deps_file:
            return json.load(deps_file)

        return None # should throw?

    def filter_modules_from_options(self, modules):
        if self.options.modules == "all":
            return modules

        required_modules = str(self.options.modules).split()
        filtered_modules = {}

        for m in required_modules:
            filtered_modules[m] = modules[m]

        return filtered_modules

    def list_libs_from_modules(self, modules):
        libs = []
        added_libs = set()

        for module in modules.values():
            for l in reversed(module['libs']):
                if l not in added_libs:
                    added_libs.add(l)

                    if self.options.version_independent_libs:
                        l = self.lib_to_alias(l)

                    libs.insert(0, l)

        return libs

    def lib_to_alias(self, lib):
        '''
        Converts a version dependent ns3 libname to
        a correspondent version independent (alias)

        Example: ns3.28-core -> ns3-core'''

        if not lib.startswith('ns3'):
            return lib

        return 'ns3' + _remove_prefix(lib, self.ns3_version_name)

    #############################

    def create_libs_alias(self, lib_dir):
        for filename in self._list_files(lib_dir):

            name = path.splitext(filename)[0]

            module = self.extract_module_name(name)

            if module:
                self.create_module_link(module, filename, lib_dir)

    def _list_files(self, dirpath):
        files = []

        for f in os.listdir(dirpath):
            p = path.join(dirpath, f)

            if path.isfile(p) and not path.islink(p):
                files.append(f)

        return files

    def extract_module_name(self, name):
        if name.startswith('libns3') is False:
            return None

        name_parts = name.split('-')

        is_dev = name.startswith('libns3-dev')
        module_start_index = 2 if is_dev else 1

        module_parts = name_parts[module_start_index:]

        if module_parts[-1] in ("debug","optimized"):
            module_parts = module_parts[:-1]

        # Rebuilds module name
        return '-'.join(module_parts)

    def create_module_link(self, module, lib_filename, libdir):
        ext = path.splitext(lib_filename)[1]

        prefix = 'lib' if lib_filename.startswith('lib') else ''

        link_name = '{}ns3-{}{}'.format(prefix, module, ext)
        link_dst = path.join(libdir, link_name)

        if path.islink(link_dst):
            self.output.info("Link to '{}' already exists".format(lib_filename))
            return

        self.output.info('create link: {} -> {}'.format(link_dst, lib_filename))
        os.symlink(lib_filename, link_dst)


 ############################### Helper classess ##################################


class Ns3Module(object):
    '''Helper class to store a ns3 module info'''

    def __init__(self, name, requires, libs):
        self.name = name
        self.requires = requires
        self.libs = self._format_libs(libs)

    def to_dict(self):
        return {
            'name': self.name,
            'requires': self.requires,
            'libs': self.libs
        }

    def _format_libs(self, libs):
        out_libs = []

        for lib in libs:
            out_libs.append(_remove_prefix(lib, '-l'))

        return out_libs


############################### Global Helpers ##################################

def _remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]

    return text
