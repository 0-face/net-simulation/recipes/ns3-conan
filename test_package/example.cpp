#include "ns3/core-module.h"

#include <iostream>

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("test_package-example");

int main (int argc, char ** argv){
    CommandLine cmd;

    cout << "*************** start example ********************" << endl;

    cout << "Parsing command line" << endl;
    cmd.Parse (argc, argv);

    cout << "Running empty simulation" << endl;
    Simulator::Stop (Seconds (20));
    Simulator::Run ();

    cout << "Destroying simulator" << endl;
    Simulator::Destroy ();

    std::cout << "*************** finish example *******************" << std::endl;

    return 0;
}
